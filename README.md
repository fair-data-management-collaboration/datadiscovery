# DataDiscovery

## Objectives
Follow-up developments on the PaN data catalogues, PaN search API and the PaN open data search portal, i.e. everything related to PaN data cataloguing and discovery.

## Useful links
- [data.panosc.eu](https://data.panosc.eu/)
- [SciCat project](https://github.com/SciCatProject)
- [ICAT project](https://github.com/icatproject)
- [PaN search API](https://github.com/panosc-eu/search-api)
- [OAI-PMH endpoint for data catalogues](https://doi.org/10.5281/zenodo.7789135)
- [Ontology API service](https://doi.org/10.5281/zenodo.7744339)
- [Search API for PaN data catalogues](https://doi.org/10.5281/zenodo.7743914)

## Description
The API developed has been integrated with the two standard ICAT and SciCat data catalogs of the PaN community and can be extended beyond that. It is complemented by a federated service demonstrator for searching open PaN data. The main feature of this service is that it allows any other facility to join the federated search thus enabling all scientists to find datasets and publications from any number of configured sites, based on metadata specific to the scientific domain concerned. 

The federated search service is accompanied by a [data portal, a web interface](https://data.panosc.eu/) already deployed and tested in partner facilities. __The search API and the underlying data model is a useful first prototype, but needs further improvement and development__. It is intended to become a tool to be maintained in future collaboration, like OSCARS.

The “search engine” it provides, gives a user the ability to enter a list of terms to search for, then a results page is presented that allows the user to refine the terms and offers further filtering options, based on the PaNOSC agreed parameters, techniques, etc. The results from the different facilities are merged and sorted based on the relevance score, described in detail in [deliverable D3.3 “Catalog Service”](https://github.com/panosc-eu/panosc/blob/master/Submitted%20Deliverables/D3.3%20Catalog%20Service/PaNOSC_D3.3_Catalogue_Service_20220321.pdf) of the PaNOSC project.
